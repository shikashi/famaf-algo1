module Lab2i5 where

------------------------------
-- Proyecto 2
-- Grupo 12
------------------------------


------------------------------
-- 5)

data ListAssoc a b = Empty | Node a b (ListAssoc a b)

-- 5) a)
type GuiaTel = ListAssoc String Int

-- 5) b)
-- la_long (la_length sería mejor)
la_long :: Integral c => ListAssoc a b -> c
la_long Empty = 0
la_long (Node _ _ next) = 1 + la_long next

-- 5) c)
-- la_aListaDePares
la_aListaDePares :: ListAssoc a b -> [(a,b)]
la_aListaDePares Empty = []
la_aListaDePares (Node x y next) = (x,y) : la_aListaDePares next

-- 5) d)
-- la_buscar
la_buscar :: Eq a => ListAssoc a b -> a -> Maybe b
la_buscar Empty _ = Nothing
la_buscar (Node x y next) s | x == s = Just y
			    | otherwise = la_buscar next s

-- 5) e)
-- La función encuentra del ejercicio 6 puede resolverse usando un tipo similar a ListAssoc.

--
------------------------------
