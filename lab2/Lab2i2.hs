module Lab2i2 where

------------------------------
-- Proyecto 2
-- Grupo 12
------------------------------


------------------------------
-- 2)

data Titulo = Ducado | Marquesado | Condado | Vizcondado | Baronia

-- 2) a)
-- hombre
hombre :: Titulo -> String
hombre Ducado = "Duque"
hombre Marquesado = "Marqués"
hombre Condado = "Conde"
hombre Vizcondado = "Vizconde"
hombre Baronia = "Barón"

-- 2) a)
-- dama
dama :: Titulo -> String
dama Ducado = "Duquesa"
dama Marquesado = "Marquesa"
dama Condado = "Condesa"
dama Vizcondado = "Vizcondesa"
dama Baronia = "Baronesa"

--
------------------------------
