module Lab2i7 where

------------------------------
-- Proyecto 2
-- Grupo 12
------------------------------

------------------------------
-- 7)

-- 7) a)
data ExprInt = Numero Integer | Opuesto ExprInt | Suma ExprInt ExprInt
	| Multiplicacion ExprInt ExprInt

-- 7) b)
data ExprArith a = Valor a | Negacion (ExprArith a) | Adicion (ExprArith a) (ExprArith a)
	| Producto (ExprArith a) (ExprArith a)
	deriving Show

-- 7) c)
-- eval
eval :: Num a => ExprArith a -> a
eval (Valor n) = n
eval (Negacion e) = -eval e
eval (Adicion e1 e2) = eval e1 + eval e2
eval (Producto e1 e2) = eval e1 * eval e2

-- 7) d)
-- Punto *
-- nnf
nnf :: Num a => ExprArith a -> ExprArith a
nnf (Negacion (Valor n)) = Valor (-n)
nnf (Negacion (Negacion e)) = nnf e
nnf (Negacion (Adicion e1 e2)) = Adicion (nnf (Negacion e1)) (nnf (Negacion e2))
nnf (Negacion (Producto e1 e2)) = Producto (nnf (Negacion e1)) (nnf e2)
nnf (Valor n) = Valor n
nnf (Adicion e1 e2) = Adicion (nnf e1) (nnf e2)
nnf (Producto e1 e2) = Producto (nnf e1) (nnf e2)

--
------------------------------
