module Lab2i1 where

------------------------------
-- Proyecto 2
-- Grupo 12
------------------------------


------------------------------
-- 1)

-- 1) a)
-- iguales
iguales :: (Eq a) => [a] -> Bool
iguales [] = True
iguales [x] = True
iguales (x:y:xs) = (x == y) && iguales (y:xs)

-- 1) b)
-- creciente
creciente :: (Ord a) => [a] -> Bool
creciente [] = True
creciente [x] = True
creciente (x:y:xs) = (x <= y) && creciente (y:xs)

-- 1) c)
-- sum_ant
sum_ant :: (Num a) => [a] -> Bool
sum_ant xs = abs_sum_ant 0 xs

-- abs_sum_ant
abs_sum_ant :: (Num a) => a -> [a] -> Bool
abs_sum_ant n [] = False
abs_sum_ant n (x:xs) = n == x || abs_sum_ant (n+x) xs

-- 1) d)
-- pos_min
pos_min :: (Num a, Ord a) => Int -> [a] -> Bool
pos_min n [] = False
pos_min 0 (x:xs) = lte_min x xs
	where
		lte_min k [] = True
		lte_min k (x:xs) = (k <= x) && lte_min k xs
pos_min (n+1) (x:xs) = ((xs !! n) <= x) && pos_min n xs
--pos_min (n+1) (x:xs) = 0 <= n && n < length xs && ((xs !! n) <= x) && pos_min n xs	{- variante: n debe estar en rango -}


-- lte_min
lte_min :: (Num a, Ord a) => a -> [a] -> Bool
lte_min k [] = True
lte_min k (x:xs) = (k <= x) && lte_min k xs

-- pos_min'
pos_min' :: (Num a, Ord a) => Int -> [a] -> Bool
pos_min' n xs = lte_min (xs !! n) xs
--pos_min' n xs = 0 <= n && n < length xs && lte_min (xs !! n) xs	{- variante: n debe estar en rango -}

-- bpm
-- better pos_min
bpm :: Int -> [Int] -> Bool
bpm n xs = bpmi xs n maxBound Nothing

-- bpmi
-- better pos_min implementation
-- TODO: notese que cuando se usa Just el valor de Just y
-- el valor del tercer argumento son iguales: se podra 
-- reducir la cantidad de parametros? -> separar bpmi en dos funciones
bpmi :: [Int] -> Int -> Int -> Maybe Int -> Bool
bpmi [x] 0 m Nothing = x <= m
bpmi [x] 0 m (Just e) = e <= x	{- e <= min x m -}
bpmi (x:xs) 0 m Nothing = x <= m && bpmi xs 0 x (Just x)	{- x <= m entonces pasar x es pasar (min x m) -}
bpmi (x:xs) 0 m (Just e) = e <= x && bpmi xs 0 e (Just e)	{- e <= x entonces e sigue siendo el minimo de todos luego pasar e es pasar (min x m) -} 
bpmi (x:xs) (n+1) m Nothing = bpmi xs n (min x m) Nothing

--
------------------------------
