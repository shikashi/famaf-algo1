module Lab2i4 where

------------------------------
-- Proyecto 2
-- Grupo 12
------------------------------


------------------------------
-- 4)

-- 4) a)

data Figura a = Triangulo a a | Rectangulo a a | Linea a | Circulo a


-- 4) b)
-- area
area :: Floating a => Figura a -> a
area (Triangulo base altura) = base * altura / 2
area (Rectangulo base altura) = base * altura
area (Linea _) = 0
area (Circulo diametro) = pi * (diametro/2)^2

--
------------------------------
