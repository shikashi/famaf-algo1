module Lab2i3 where

------------------------------
-- Proyecto 2
-- Grupo 12
------------------------------

import Lab2i2 (Titulo, hombre, dama)

------------------------------
-- 3)

type Territorio = String
type Nombre = String

data Persona = Rey | Noble Titulo Territorio | Caballero Nombre | Aldeano Nombre

data Genero = Masculino | Femenino

data Persona' = Monarca Genero | Noble' Genero Titulo Territorio
	| Caballero' Nombre | Aldeano' Nombre

data Persona'' = Individuo Persona Genero

-- 3) a)
-- tratamiento
tratamiento :: Persona -> String
tratamiento Rey = "Su majestad el rey"
tratamiento (Noble ti te) = "El " ++ hombre ti ++ " de " ++ te
tratamiento (Caballero n) = "Sir " ++ n
tratamiento (Aldeano n) = n

-- 3) b)
-- tratamiento'z
tratamiento' :: Persona' -> String
tratamiento' (Monarca Masculino) = "Su majestad el rey"
tratamiento' (Monarca Femenino) = "Su majestad la reina"
tratamiento' (Noble' Masculino ti te) = "El " ++ hombre ti ++ " de " ++ te
tratamiento' (Noble' Femenino ti te) = "La " ++ dama ti ++ " de " ++ te
tratamiento' (Caballero' n) = "Sir " ++ n
tratamiento' (Aldeano' n) = n

-- tratamiento''
tratamiento'' :: Persona'' -> String
tratamiento'' (Individuo Rey Masculino) = "Su majestad el rey"
tratamiento'' (Individuo Rey Femenino) = "Su majestad la reina"
tratamiento'' (Individuo (Noble ti te) Masculino) = "El " ++ hombre ti ++ " de " ++ te
tratamiento'' (Individuo (Noble ti te) Femenino) = "La " ++ dama ti ++ " de " ++ te
tratamiento'' (Individuo (Caballero n) Masculino) = "Sir " ++ n
tratamiento'' (Individuo (Caballero n) Femenino) = error "No existen mujeres caballeros."
tratamiento'' (Individuo (Aldeano n) _) = n

-- 3) c)
-- sirs
sirs :: [Persona] -> [String]
sirs [] = []
sirs ((Caballero n):xs) = n:sirs xs
sirs (_:xs) = sirs xs

-- 3) d)
-- sirs'
sirs' :: [Persona] -> [String]
sirs' xs = map nom (filter cab xs)
	where
		nom (Caballero n) = n
		cab (Caballero _) = True
		cab _ = False

--
------------------------------
