module Lab2i6 where

------------------------------
-- Proyecto 2
-- Grupo 12
------------------------------

import Lab2i3 (tratamiento, Persona)

------------------------------
-- 6)

data Arbol a = Hoja | Rama (Arbol a) a (Arbol a)
	deriving (Show)

-- 6) a)
-- aLargo
aLargo :: Integral b => Arbol a -> b
aLargo Hoja = 0
aLargo (Rama izq _ der) = aLargo izq + 1 + aLargo der


-- 6) b)
-- aCantHojas
aCantHojas :: Integral b => Arbol a -> b
aCantHojas Hoja = 1
aCantHojas (Rama izq _ der) = aCantHojas izq + aCantHojas der

-- 6) c)
-- aInc
aInc :: Num a => Arbol a -> Arbol a
aInc Hoja = Hoja
aInc (Rama izq k der) = Rama (aInc izq) (k + 1) (aInc der)

-- 6) d)
-- aTratamiento
aTratamiento :: Arbol Persona -> Arbol String
aTratamiento Hoja = Hoja
aTratamiento (Rama izq p der) = Rama (aTratamiento izq) (tratamiento p) (aTratamiento der)

-- 6) e)
-- aMap
aMap :: (a -> b) -> Arbol a -> Arbol b
aMap f Hoja = Hoja
aMap f (Rama izq r der) = Rama (aMap f izq) (f r) (aMap f der)

-- 6) f)
-- aInc'
aInc' :: Num a => Arbol a -> Arbol a
aInc' t = aMap (+1) t

-- aTratamiento'
aTratamiento' :: Arbol Persona -> Arbol String
aTratamiento' t = aMap tratamiento t

-- 6) g)
-- aSum
aSum :: Num a => Arbol a -> a
aSum Hoja = 0
aSum (Rama izq n der) = aSum izq + n + aSum der

-- 6) h)
-- aProd
aProd :: Num a => Arbol a -> a
aProd Hoja = 1
aProd (Rama izq n der) = aProd izq * n * aProd der

-- 6) i)
-- Punto *
-- aFold
aFold :: (b -> a -> b -> b) -> b -> Arbol a -> b
aFold op z Hoja = z
aFold op z (Rama izq p der) = op (aFold op z izq) p (aFold op z der)

-- aLargoFold
aLargoFold :: Integral b => Arbol a -> b
aLargoFold t = aFold sum3 0 t
	where
		sum3 x _ y = x + 1 + y

-- aCantHojasFold
aCantHojasFold :: Integral b => Arbol a -> b
aCantHojasFold t = aFold sum3 1 t
	where
		sum3 x _ y = x + 0 + y


-- aSumFold
aSumFold :: Num a => Arbol a -> a
aSumFold t = aFold sumaux 0 t
 	where
		sumaux x y z = x + y + z

-- aProdFold
aProdFold :: Num a => Arbol a -> a
aProdFold t = aFold prodaux 1 t
 	where
		prodaux x y z = x * y * z

-- aMapFold
aMapFold :: (a -> b) -> Arbol a -> Arbol b
aMapFold f t = aFold aux Hoja t
	where
		aux izq p der = Rama izq (f p) der

--
------------------------------
