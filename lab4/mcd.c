/**************************************************************\
 * Proyecto 4
 * Grupo 12
 * Ejercicio 4
\**************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	unsigned int x, y;

	printf("Introduzca dos números positivos: ");
	scanf("%u%u", &x, &y);

	// { x > 0 && y > 0 && x = X && y = Y }

	if (x == 0 || y == 0)
	{
		printf("MAL!\n");
		abort();
	}

	while ((x < y) || (y < x)) 
	{
		if (x < y)
			y -= x;
		
		else if (y < x)
			x -= y;
	}

	printf("El m.c.d. es %d\n", x);

	return EXIT_SUCCESS;
}
