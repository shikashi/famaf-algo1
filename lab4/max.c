/**************************************************************\
 * Proyecto 4
 * Grupo 12
 * Ejercicio 1
\**************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int a, b, res;

	printf("Ingrese dos números, por favor: ");
	// scanf("%d%d", &a, &b);
	scanf("%d", &a);
	scanf("%d", &b);

	// { pre: a = A && b = B }

	if (a >= b)
		res = a;
	else
		res = b;

	printf("El máximo es: %d\n", res);
	
	// { pos: res = max(A,B) }

	return EXIT_SUCCESS;
}
