/*******************************
 * Proyecto 4
 * Grupo 12
 * Ejercicio 14
 * Punto estrella (*)
*******************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main ()
{
	int k,i;
	size_t N;
	bool b;
	char* a;
	char fmt[10] = { '\0' };

	//N = sizeof (a) / sizeof (a[0]);

	b = true;
	k = 0;
	i = 0;

	printf("Longitud de la cadena: ");
	scanf ("%u", &N);

	a = malloc(N + 1);
	if (a == NULL) {
		printf("Sin memoria suficiente!");
		return -1;
	}

	//fflush(stdin); // fflush cannot be used for input streams
	scanf("%*c");

	sprintf(fmt, "%%%dc", N);

	printf("Ingrese una formula matemática (longitud máxima: %d): ", N);
	scanf(fmt, a);
	a[N] = '\0';

	while (i != N) {
		if (a[i] == '(') {
			b = b && (k >= 0);
			++k;
			++i;
		} else if (a[i] == ')') {
			b = b && (k >= 0);
			--k;
			++i;
		} else {
			++i;
		}
	}

	free(a);
	
	if (b && (k == 0)) {
		printf ("La fórmula está balanceada en paréntesis\n");
	} else {
		printf ("La fórmula no está balanceada en paréntesis\n");
	}

	return EXIT_SUCCESS;
}
