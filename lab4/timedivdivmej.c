/**************************************************************\
 * Proyecto 4
 * Grupo 12
 * Ejercicio 9
 * Punto estrella (*)
\**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void div1(int, int);

void divmej(int, int);

int main()
{
	int x, y;
	clock_t ini1, ini2, fin1, fin2;
	double dif1, dif2;

	printf("Dividendo: ");
	scanf("%d", &x);
	printf("Divisor: ");
	scanf("%d", &y);

	if (y == 0) {
		printf("MAL! ");
		abort();
	}

	printf("Vamos a probar 100000 casos, con un dividendo que varíe entre %d y %d, y con un divisor que varíe entre %d y %d.\n", x, x+9999, y, y+9);

	ini1 = clock();
	for (int i = x; i < x + 9999; i++)
		for (int j = y; j < y + 9; j++)
			div1(i, j);
	fin1 = clock();

	dif1 = fin1 - ini1;

	printf("La función div tardó %lf segundos. ", dif1 / CLOCKS_PER_SEC);

	ini2 = clock();
	for (int k = x; k < x + 9999; k++)
		for (int l = y; l < y + 9; l++)
			divmej(k, l);
	fin2 = clock();

	dif2 = fin2 - ini2;

	printf("La funcion divmej tardó %lf segundos. Luego, ", dif2 / CLOCKS_PER_SEC);

	if (dif1 < dif2)
		printf("div es más rápida.\n");

	else
		printf("divmej es más rápida.\n");

	return 0;
}	

void div1(int x, int y)
{
	unsigned int q, r;

	// { pre: x >= 0 && y > 0 }

	q = 0;
	r = x;

	// { inv: x = q * y + r && 0 <= r }
	// ( cot: t.r = r }

	while (r >= y)
	{
		++q;
		r -= y;
	}

	// { pos: x = q * y + r && 0 <= r && r < y }

	return;
}

void divmej(int x, int y)
{
	int q = 0, r, d, dd;

	r = x;

	while (r >= y) {
		d = 1;
		dd = y;
		while (r >= 2 * dd){
			d *= 2;
			dd *= 2;
		}
		q += d;
		r -= dd;
	}

	return;
}
