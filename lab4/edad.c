/**************************************************************\
 * Proyecto 4
 * Grupo 12
 * Ejercicio 2
\**************************************************************/

#include <stdio.h>
#include <stdlib.h>


int main(void)
{
	int d1, d2, m1, m2, a1, a2, res;

	printf("Ingrese una fecha, por favor (d/m/a): ");
	scanf("%d/%d/%d", &d1, &m1, &a1);
	printf("Ahora ingrese una fecha POSTERIOR, por favor (d/m/a): ");
	scanf("%d/%d/%d", &d2, &m2, &a2);

	// { pre: fecha(a1,m1,d1) >= fecha(a2,m2,d2) }
	
	if (a1 < a2 || (a1 == a2 && m1 < m2) || (a1 == a2 && m1 == m2 && d1 < d2)) {
		printf("MAL!");
		abort();
		//exit(EXIT_FAILURE)
	}

	if ((m1 < m2) || ((m1 == m2) && (d1 < d2)))
		res = a2 - a1;
	else if ((m1 > m2) || ((m1 == m2) && (d1 >= d2)))
		res = a2 - a1 - 1;

	printf("Transcurrieron %d años.\n", res);
	
	// { pos: res = (fecha(a1,m1,d1) - fecha(a2,m2,d2)).año }

	return EXIT_SUCCESS;
}
