/**************************************************************\
 * Proyecto 4
 * Grupo 12
 * Ejercicio 12
\**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (void)
{
	int r, u, i, N;
	int a[20];
	
	N = sizeof (a) / sizeof (a[0]);
	i = 0;
	r = 0;
	u = 0;
	
	printf("Ingrese una lista separada por comas");
	scanf ("%d", a);
	
	while (i != N) {
		r = (int)fmaxf(r, u);
		u = (int)fmaxf(a[i] + u, 0);
		i = i + 1;
	}
	
	printf ("La respuesta es %d \n", r);
	
	return EXIT_SUCCESS;
}