/**************************************************************\
 * Proyecto 4
 * Grupo 12
 * Ejercicio 13
\**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef N
#	define N 100
#endif

int main ()
{

	int a[N], i = 0, j = 0, r = 0;
	bool b = false;

	printf("Ingrese una lista de números enteros (máximo 100 elementos): ");

	while (i < N)
	{
		r = scanf("%d", &a[i]);
		if (r == EOF)
			break;
		++i;
	}

	while (j < i && !b)
	{
		printf("%d\n", a[j]);
		b = b || (a[j] > 0);
		++j;
	}

	if (b)
		printf("La respuesta es True\n");
	else
		printf("La respuseta es False\n");

	return 0;
}
