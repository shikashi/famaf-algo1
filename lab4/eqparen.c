/**************************************************************\
 * Proyecto 4
 * Grupo 12
 * Ejercicio 14
\**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main (void)
{
	int k,i,N;
	bool b;
	char a[20];

	N = sizeof (a) / sizeof (a[0]);

	b = true;
	k = 0;
	i = 0;

	printf("Ingrese una formula matemática: ");
	scanf ("%s", a);
	
	while (i != N) {
		if (a[i] == '(') {
			b = b && (k >= 0);
			++k;
			++i;
		} 
		else if (a[i] == ')') {
			b = b && (k >= 0);
			--k;
			++i;
	        }
	        else {
			++i;
	        }
	}
	
	if (b && (k == 0)) {
		printf ("La fórmula está balanceada en paréntesis.\n");
	} else {
		printf ("La fórmula no está balanceada en paréntesis.\n");
	}
	return EXIT_SUCCESS;
}