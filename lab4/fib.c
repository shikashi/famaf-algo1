/*******************************
 * Proyecto 4
 * Grupo 12
 * Ejercicio 11
*******************************/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	unsigned int r, y, n, rr = 0, m;

	r = 0;
	y = 1;
	n = 0;

	printf("Ingrese el valor n para calcular fib.n: ");
	scanf("%u", &m);

	do {			    //do n != N → r, y, n := y, y + r, n + 1 od	
		rr = r;
		r = y;
		y += rr;
		++n;
	} while (n != m);

	printf("El término %d-ésimo de fibonacci es: %d\n", m, r);

	return 0;
}