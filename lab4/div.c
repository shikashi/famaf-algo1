/**************************************************************\
 * Proyecto 4
 * Grupo 12
 * Ejercicio 3
\**************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	unsigned int x, y, q, r;

	printf("Dividendo: ");
	scanf("%u", &x);
	printf("Divisor: ");
	scanf("%u", &y);

	// { pre: x >= 0 && y > 0 }

	if (y == 0) {
		printf("MAL!");
		abort();
	}

	q = 0;
	r = x;

	// { inv: x = q * y + r && 0 <= r }
	// ( cot: t.r = r }

	while (r >= y) {
		++q;
		r -= y;
	}

	printf("Cociente entero: %u\n", q);
	printf("Resto: %u\n", r);
	
	// { pos: x = q * y + r && 0 <= r && r < y }

	return EXIT_SUCCESS;
}
