--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module AbbContainer (
	Abb,
	Container(..)
) where

--
--------------------------------------------------------------------------------
-- Imports

import Abb

import Container

--
--------------------------------------------------------------------------------
-- Class membership

instance Container Abb where
	empty = abb_empty
	add = abb_add
	search = abb_search
	del = abb_del
	toListPair = abb_toListPair

--
--------------------------------------------------------------------------------
