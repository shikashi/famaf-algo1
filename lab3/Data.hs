--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Data (
	Data,
	data_fromString,
	data_toString,
	data_length
) where

--
--------------------------------------------------------------------------------
-- Types

data Data = DataVal String Int
	deriving (Show, Eq) -- needed by the testing functions

--
--------------------------------------------------------------------------------
-- Functions

-- data_fromString
-- De un String construye un Data
data_fromString :: String -> Data
data_fromString str = DataVal str (length str)

-- data_toString
-- Convierte un Data a String
data_toString :: Data -> String
data_toString (DataVal str _) = str

-- data_length
-- Devuelve el tamaño del Data
data_length :: Data -> Int
data_length (DataVal _ len) = len

--
--------------------------------------------------------------------------------
