--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module ListAssocOrd (
	ListAssoc,
	la_empty,
	la_add,
	la_search,
	la_del,
	la_toListPair
) where

--
--------------------------------------------------------------------------------
-- Imports

-- El tipo ListAssoc lo tomamos del módulo ListAssocType (en lugar de importarlo
-- del módulo ListAssoc) porque necesitamos acceso a los constructores.
import ListAssocType

-- La implementación de estas dos funciones no cambia para nuestra lista de
-- asociaciones ordenada, así que las tomamos directamente del módulo ListAssoc.
import ListAssoc ( la_empty, la_toListPair )

--
--------------------------------------------------------------------------------
-- Functions

-- la_add
-- Agrega un elemento a una lista de asociaciones
-- Si el elemento ya existe, devuelve la misma lista
-- Nota: dos elementos son iguales cuando la primera componente de cada elemento coincide
la_add :: Ord a => ListAssoc a b -> a -> b -> ListAssoc a b
la_add Empty x y = Node x y Empty
la_add (Node u v next) x y
	| x == u = Node u v next
	| x > u = Node u v (la_add next x y)
	| x < u = Node x y (Node u v next)

-- la_search
-- Devuelve el elemento asociado
-- Si el elemento no existe, devuelve Nothing
la_search :: Ord a => ListAssoc a b -> a -> Maybe b
la_search Empty _ = Nothing
la_search (Node u v next) x
	| x == u = Just v
	| x > u = la_search next x
	| x < u = Nothing

-- la_del
-- Borra un elemento de la lista de asociaciones
-- Si el elemento no existe, hace nada
la_del :: Ord a => ListAssoc a b -> a -> ListAssoc a b
la_del Empty _ = Empty
la_del (Node u v next) x
	| x == u = next
	| x > u = Node u v (la_del next x)
	| x < u = Node u v next

--
--------------------------------------------------------------------------------
