--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module ListAssocType where

--
-- Este módulo sólo declara el tipo ListAssoc y lo exporta junto con sus
-- constructores. Esto resulta útil porque más tarde querremos construir
-- los módulos ListAssoc y ListAssocOrd de forma acumulativa, sin embargo,
-- por requerimiento del proyecto, ninguno de esos módulos puede exportar
-- los constructores del tipo ListAssoc.
--

--
--------------------------------------------------------------------------------
-- Types

data ListAssoc a b = Empty | Node a b (ListAssoc a b)
	deriving (Show, Eq) -- needed by the testing functions

--
--------------------------------------------------------------------------------
