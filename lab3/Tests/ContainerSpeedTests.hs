--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Tests.ContainerSpeedTests where

--
--------------------------------------------------------------------------------
-- Imports

-- Containers
import qualified ListAssoc as ListAssocEq
import qualified ListAssocOrdContainer as ListAssocOrd
import AbbContainer

-- Elements
import Key
import Data

-- From these modules we only want the Arbitrary instantiation
import Tests.KeyTests ( )
import Tests.DataTests ( )

-- Arbitrary class, generator combinators
import Test.QuickCheck

-- Utilities
import Utils

-- Monadic tools
import Control.Monad ( liftM, liftM2, liftM3 )

-- Timing
import System.CPUTime

-- Formatted Output
import Text.Printf

-- stdout is not tied to stdin... -_-!
import System.IO ( stdout, BufferMode(..), hSetBuffering )

--
--------------------------------------------------------------------------------
-- Functions

main :: IO ()
main = do
	hSetBuffering stdout NoBuffering -- stdout is line-buffered by default, and it's not tied to/synced with stdin!! we make it unbuffered instead to avoid further hassle
	putStr "Enter the length of the generated container (Int): "
	n <- readLn
	putStr "Enter the key to look for (String): "
	key <- getLine
	testPerformance n key "whatever" -- the data value makes no difference

-- testPerformance
-- Generate n random elements. Add them together with the (ks,ds) element
-- to a container of each type. Finally, search for the ks key. Measure the
-- speed at each step.
testPerformance :: Int -> String -> String -> IO ()
testPerformance n ks ds = do
	putStr $ "Generating " ++ show n ++ " random elements... "
	(t1,xs) <- timeAndThunk getCPUTime do_list
	t1 <- timeThunk getCPUTime (return $ length xs) -- hack: stupid lazy evaluation!
	done_msg t1

	putStrLn "Adding elements to containers..."

	putStr "\tListAssoc(Eq)... "
	(t1,lae) <- timeAndThunk getCPUTime (return $ do_lae xs)
	done_msg t1

	putStr "\tListAssoc(Ord)... "
	(t1,lao) <- timeAndThunk getCPUTime (return $ do_lao xs)
	done_msg t1

	putStr "\tAbb... "
	(t1,abb) <- timeAndThunk getCPUTime (return $ do_abb xs)
	done_msg t1

	putStrLn $ "Searching for key '" ++ ks ++ "'..."

	putStr "\tListAssoc(Eq)... "
	t1 <- timeThunk getCPUTime (return $ do_search_lae lae)
	done_msg t1

	putStr "\tListAssoc(Ord)... "
	t1 <- timeThunk getCPUTime (return $ do_search_co lao)
	done_msg t1

	putStr "\tAbb... "
	t1 <- timeThunk getCPUTime (return $ do_search_co abb)
	done_msg t1

	where
		k = key_fromString ks
		d = data_fromString ds

		do_list = liftM (!! 10) (sample' $ vectorOf n pairGen) -- ugly. also: sample' returns a list of 11 elements, a higher list index means a bigger size parameter for the given generator, we use 10 because we want longer strings for Key and Data
		pairGen =  arbitrary :: Gen (Key,Data)

		do_lae ls = ListAssocEq.la_add (foldl3 ListAssocEq.la_add ListAssocEq.la_empty ls) k d
		do_search_lae la = ListAssocEq.la_search la k

		do_lao ls = add (foldl3 add empty ls) k d :: ListAssocOrd.ListAssoc Key Data
		do_abb ls = add (foldl3 add empty ls) k d :: Abb Key Data
		do_search_co co = search co k

		secsf t = printf "%0.6f secs" (fromPico t :: Double)
		done_msg t = putStrLn $ "Done in " ++ secsf t ++ "."

--
--------------------------------------------------------------------------------
