--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Tests.ContainerCommon where

--
--------------------------------------------------------------------------------
-- Imports

import Container

-- Utilities
import Utils

-- Arbitrary class, generator combinators, testing
import Test.QuickCheck
import Test.QuickCheck.Test ( isSuccess )

-- Monadic tools
import Control.Monad ( liftM, liftM2, liftM3 )

-- Timing
import System.CPUTime

-- Formatted output
import Text.Printf

--
--------------------------------------------------------------------------------
-- Generators

-- genSimple
-- Flip a coin.
genSimple ::
	(Container c, Arbitrary (c a b), Arbitrary a, Ord a, Arbitrary b) =>
	Gen (c a b)
genSimple = do
	i <- choose (0,1) :: Gen Int
	case i of
		0 -> return empty
		1 -> liftM3 add arbitrary arbitrary arbitrary

-- genCtrlFreq
-- We prefer some sort of control over the frequency.
genCtrlFreq ::
	(Container c, Arbitrary (c a b), Arbitrary a, Ord a, Arbitrary b) =>
	(Int -> Int) -> Gen (c a b)
genCtrlFreq f = sized crtl_freq
	where
		crtl_freq n = frequency
			[ (1, return empty)
			, (f n, liftM3 add arbitrary arbitrary arbitrary)
			]

-- genFromListOf
-- Generate a list of (a,b) taken from generator g, then fold it into a
-- container type.
genFromListOf :: (Container c, Ord a) => Gen (a,b) -> Gen (c a b)
genFromListOf g = liftM toContainer (listOf g)
	where
		toContainer xs = foldl3 add empty xs

-- genFromList
-- Same as genFromListOf but implicitly using an 'arbitrary' generator.
genFromList :: (Container c, Arbitrary a, Ord a, Arbitrary b) => Gen (c a b)
genFromList = genFromListOf arbitrary

-- genVectorOf
-- Generate a list of (a,b) of length n taken from generator g, then fold it
-- into a container type.
genVectorOf :: (Container c, Ord a) => Int -> Gen (a,b) -> Gen (c a b)
genVectorOf n g = liftM toContainer (vectorOf n g)
	where
		toContainer xs = foldl3 add empty xs

-- genVector
-- Same as genVectorOf but implicitly using an 'arbitrary' generator.
genVector :: (Container c, Arbitrary a, Ord a, Arbitrary b) => Int -> Gen (c a b)
genVector n = genVectorOf n arbitrary

--
--------------------------------------------------------------------------------
-- Tests

-- defaultMain
defaultMain :: Testable prop => [prop] -> IO ()
defaultMain ps = do
	r <- checkDriver ps
	if r
		then putStrLn "All tests completed successfully."
		else putStrLn "Some tests failed!"

-- checkDriver
checkDriver :: Testable prop => [prop] -> IO Bool
checkDriver = checkDriverWith args
	where
		args = stdArgs { maxSuccess = 200 }

-- checkDriverWith
checkDriverWith :: Testable prop => Args -> [prop] -> IO Bool
checkDriverWith args ps = do
	rs <- mapM (quickCheckWithResult args) ps
	return $ all (isSuccess) rs

--
--------------------------------------------------------------------------------
