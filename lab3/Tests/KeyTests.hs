--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Tests.KeyTests where

--
--------------------------------------------------------------------------------
-- Imports

import Key

-- Arbitrary class, generator combinators, testing
import Test.QuickCheck
import Test.QuickCheck.Test ( isSuccess )

--
--------------------------------------------------------------------------------
-- Invariants/Properties

-- prop_lengthMax
prop_lengthMax :: Key -> Bool
prop_lengthMax k = key_length k <= key_MaxLen

-- prop_fromToId
prop_fromToId :: Key -> Bool
prop_fromToId k = key_fromString (key_toString k) == k

--
--------------------------------------------------------------------------------
-- Class membership

instance Arbitrary Key where
	arbitrary = do
		str <- suchThat (listOf $ elements nicechars) (\s -> length s <= key_MaxLen)
		return $ key_fromString str
		where
			nicechars = ['0' .. '9'] ++ ['a' .. 'z'] ++ ['A' .. 'Z']

--
--------------------------------------------------------------------------------
-- Tests

main :: IO ()
main = do
	rs <- mapM do_check
		[ prop_lengthMax
		, prop_fromToId
		]
	if all (isSuccess) rs
		then putStrLn "All tests completed successfully."
		else putStrLn "Some tests failed!"
	where
		do_check = quickCheckWithResult args
		args = stdArgs { maxSuccess = 200 }

--
--------------------------------------------------------------------------------
