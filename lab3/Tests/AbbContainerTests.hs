
--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Tests.AbbContainerTests where

--
--------------------------------------------------------------------------------
-- Imports

import AbbContainer

-- Elements
import Key
import Data

-- From these modules we only want the Arbitrary instantiation
import Tests.KeyTests ()
import Tests.DataTests ()

-- Utilities
import Utils ( foldl3 )

-- Arbitrary class
import Test.QuickCheck.Arbitrary

-- Common testing stuff for members of Container
import Tests.ContainerCommon

--
--------------------------------------------------------------------------------
-- Invariants/Properties

-- prop_addDel
-- En realidad, este predicado está asumiendo que las operaciones 'add' y
-- 'del' son estables...
--prop_addDel :: Abb a b -> Bool
prop_addDel :: Abb Key Data -> Bool
prop_addDel abb = del (add abb k d) k == abb
	where
		k = key_fromString "HERE"
		d = data_fromString "NOW"

-- prop_toListAndBack
-- De nuevo, este predicado está asumiendo que las operaciones 'add' y
-- 'toListPair' son estables...
prop_toListAndBack :: (Ord a, Eq b) => Abb a b -> Bool
prop_toListAndBack abb = foldl3 add empty (toListPair abb) == abb

--
--------------------------------------------------------------------------------
-- Class membership

instance (Arbitrary a, Ord a, Arbitrary b) => Arbitrary (Abb a b) where
	arbitrary = genCtrlFreq (\x -> round $ sqrt $ fromIntegral x)

--
--------------------------------------------------------------------------------
-- Tests

main :: IO ()
main = defaultMain
	[ prop_addDel
	, prop_toListAndBack
	]

--
--------------------------------------------------------------------------------

