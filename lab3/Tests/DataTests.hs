--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Tests.DataTests where

--
--------------------------------------------------------------------------------
-- Imports

import Data

-- Arbitrary class, generator combinators, testing
import Test.QuickCheck
import Test.QuickCheck.Test ( isSuccess )

--
--------------------------------------------------------------------------------
-- Invariants/Properties

-- prop_fromToId
prop_fromToId :: Data -> Bool
prop_fromToId d = data_fromString (data_toString d) == d

--
--------------------------------------------------------------------------------
-- Class membership

instance Arbitrary Data where
	arbitrary = do
		str <- listOf $ elements nicechars
		return $ data_fromString str
		where
			nicechars = ['0' .. '9'] ++ ['a' .. 'z'] ++ ['A' .. 'Z']

--
--------------------------------------------------------------------------------
-- Tests

main :: IO ()
main = do
	rs <- mapM do_check
		[ prop_fromToId
		]
	if all (isSuccess) rs
		then putStrLn "All tests completed successfully."
		else putStrLn "Some tests failed!"
	where
		do_check = quickCheckWithResult args
		args = stdArgs { maxSuccess = 200 }

--
--------------------------------------------------------------------------------
