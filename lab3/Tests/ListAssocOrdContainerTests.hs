
--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Tests.ListAssocOrdContainerTests where

--
--------------------------------------------------------------------------------
-- Imports

import ListAssocOrdContainer

-- Elements
import Key
import Data

-- From these modules we only want the Arbitrary instantiation
import Tests.KeyTests ( )
import Tests.DataTests ( )

-- Utilities
import Utils ( foldl3 )

-- Arbitrary class
import Test.QuickCheck.Arbitrary

-- Common testing stuff for members of Container
import Tests.ContainerCommon

--
--------------------------------------------------------------------------------
-- Invariants/Properties

-- prop_addDel
-- En realidad, este predicado está asumiendo que las operaciones 'add' y
-- 'del' son estables...
--prop_addDel :: Eq a => ListAssoc a b -> Bool
prop_addDel :: ListAssoc Key Data -> Bool
prop_addDel la = del (add la k d) k == la
	where
		k = key_fromString "HERE"
		d = data_fromString "NOW"

-- prop_toListAndBack
-- De nuevo, este predicado está asumiendo que las operaciones 'add' y
-- 'toListPair' son estables...
prop_toListAndBack :: (Ord a, Eq b) => ListAssoc a b -> Bool
prop_toListAndBack la = foldl3 add empty (toListPair la) == la

-- prop_sorted
-- Este predicado está asumiendo que la función de orden es "<=".
prop_sorted :: Ord a => ListAssoc a b -> Bool
prop_sorted la = ordered $ toListPair la
	where
		ordered [] = True
		ordered [x] = True
		ordered ((x,_):p@(y,_):xs) = x <= y && ordered (p:xs)

--
--------------------------------------------------------------------------------
-- Class membership

instance (Arbitrary a, Ord a, Arbitrary b) => Arbitrary (ListAssoc a b) where
	arbitrary = genCtrlFreq (\x -> round $ sqrt $ fromIntegral x)

--
--------------------------------------------------------------------------------
-- Tests

main :: IO ()
main = defaultMain
	[ prop_addDel
	, prop_toListAndBack
	, prop_sorted
	]

--
--------------------------------------------------------------------------------

