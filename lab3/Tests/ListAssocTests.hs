
--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Tests.ListAssocTests where

--
--------------------------------------------------------------------------------
-- Imports

import ListAssoc

-- Elements
import Key
import Data

-- From these modules we only want the Arbitrary instantiation
import Tests.KeyTests ( )
import Tests.DataTests ( )

-- Utilities
import Utils ( foldl3 )

-- Arbitrary class, generator combinators, testing
import Test.QuickCheck
import Test.QuickCheck.Test ( isSuccess )

-- Monadic tools
import Control.Monad ( liftM, liftM2, liftM3 )

--
--------------------------------------------------------------------------------
-- Invariants/Properties

-- ¿Deberían estos predicados tener permitido hacer asunciones respecto de
-- cómo está implemetado el tipo sobre el cual realizan una aserción? Por caso,
-- ¿debería permitirse usar los constructores del tipo ListAssoc?
-- Rta: Existen distintos tipos de invariantes, invariantes de tipo abstracto e
-- invariantes de implementación, por caso. Entonces puede uno tener propiedades
-- o invariantes que verifiquen la validez de uno u otro caso.

-- prop_addDel
-- En realidad, este predicado está asumiendo que las operaciones 'la_add'
-- y 'la_del' son estables...
--prop_addDel :: Eq a => ListAssoc a b -> Bool
prop_addDel :: ListAssoc Key Data -> Bool
prop_addDel la = la_del (la_add la k d) k == la
	where
		k = key_fromString "HERE"
		d = data_fromString "NOW"

-- prop_toListAndBack
-- De nuevo, este predicado está asumiendo que las operaciones 'la_add'
-- y 'la_toListPair' son estables...
prop_toListAndBack :: (Eq a, Eq b) => ListAssoc a b -> Bool
prop_toListAndBack la = foldl3 la_add la_empty (la_toListPair la) == la

--
--------------------------------------------------------------------------------
-- Generators

-- la_genSimple
-- Flip a coin.
la_genSimple :: (Arbitrary a, Eq a, Arbitrary b) => Gen (ListAssoc a b)
la_genSimple = do
	i <- choose (0,1) :: Gen Int
	case i of
		0 -> return la_empty
		1 -> liftM3 la_add arbitrary arbitrary arbitrary

-- la_genCtrlFreq
-- We prefer some sort of control over the frequency.
la_genCtrlFreq :: (Arbitrary a, Eq a, Arbitrary b) => (Int -> Int) -> Gen (ListAssoc a b)
la_genCtrlFreq f = sized crtl_freq
	where
		crtl_freq n = frequency
			[ (1, return la_empty)
			, (f n, liftM3 la_add arbitrary arbitrary arbitrary)
			]

-- la_genFromListOf
-- Generate a list of (a,b) taken from generator g, then fold it into a
-- ListAssoc.
la_genFromListOf :: Eq a => Gen (a,b) -> Gen (ListAssoc a b)
la_genFromListOf g = liftM toLA (listOf g)
	where
		toLA xs = foldl3 la_add la_empty xs

-- la_genFromList
-- Same as la_genFromListOf but implicitly using an 'arbitrary' generator.
la_genFromList :: (Arbitrary a, Eq a, Arbitrary b) => Gen (ListAssoc a b)
la_genFromList = la_genFromListOf arbitrary

-- la_genVectorOf
-- Generate a list of (a,b) of length n taken from generator g, then fold it
-- into a ListAssoc.
la_genVectorOf :: Eq a => Int -> Gen (a,b) -> Gen (ListAssoc a b)
la_genVectorOf n g = liftM toLA (vectorOf n g)
	where
		toLA xs = foldl3 la_add la_empty xs

-- la_genVector
-- Same as la_genVectorOf but implicitly using an 'arbitrary' generator.
la_genVector :: (Arbitrary a, Eq a, Arbitrary b) => Int -> Gen (ListAssoc a b)
la_genVector n = la_genVectorOf n arbitrary

--
--------------------------------------------------------------------------------
-- Class membership

instance (Arbitrary a, Eq a, Arbitrary b) => Arbitrary (ListAssoc a b) where
	arbitrary = la_genCtrlFreq (\x -> round $ sqrt $ fromIntegral x)

--
--------------------------------------------------------------------------------
-- Tests

main :: IO ()
main = do
	rs <- mapM do_check
		[ prop_addDel
		, prop_toListAndBack
		]
	if all (isSuccess) rs
		then putStrLn "All tests completed successfully."
		else putStrLn "Some tests failed!"
	where
		do_check = quickCheckWithResult args
		args = stdArgs { maxSuccess = 200 }

--
--------------------------------------------------------------------------------

