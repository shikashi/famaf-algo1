--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Dict (
	Dict,
	Word,
	Def,
	dict_empty,
	dict_add,
	dict_exist,
	dict_search,
	dict_del,
	dict_length,
	dict_toDefs
) where

--
--------------------------------------------------------------------------------
-- Imports

import Data

import Key

import ListAssoc

--
--------------------------------------------------------------------------------
-- Types

-- se almacena el tamaño
data Dict = Dict Int (ListAssoc Key Data)

type Word = String

type Def = String

--
--------------------------------------------------------------------------------
-- Functions

-- dict_empty
-- Crea un diccionario vacío
dict_empty :: Dict
dict_empty = Dict 0 la_empty

-- dict_add
-- Agrega un elemento al diccionario
-- Si la clave ya existe, devuelve error
dict_add :: Dict -> Word -> Def -> Dict
dict_add (Dict n la) w d = case la_search la (key_fromString w) of
	Nothing -> Dict (n + 1) (la_add la (key_fromString w) (data_fromString d))
	Just _ -> error "Key already in dictionary."

-- dict_exist
-- Pregunta si la clave existe en el diccionario
dict_exist :: Dict -> Word -> Bool
dict_exist (Dict 0 _) _ = False
dict_exist (Dict _ la) w = case la_search la (key_fromString w) of
	Nothing -> False
	otherwise -> True

-- dict_search
-- Devuelve el dato asociado a una clave
-- Si la clave no existe, devuelve Nothing
dict_search :: Dict -> Word -> Maybe Def
dict_search (Dict _ la) w = case la_search la (key_fromString w) of
	Nothing -> Nothing
	Just d -> Just (data_toString d)

-- dict_del
-- Borra un elemento del diccionario a partir de una clave
-- Si la clave no existe, devuelve error
dict_del :: Dict -> Word -> Dict
dict_del (Dict n la) w = case la_search la (key_fromString w) of
	Nothing -> error "Key does not exist."
	Just _ -> Dict (n - 1) (la_del la (key_fromString w))

-- dict_length
-- Devuelve la cantidad de elementos en el diccionario
dict_length :: Dict -> Int
dict_length (Dict n _) = n

-- dict_toDefs
-- Devuelve las definiciones del diccionario
dict_toDefs :: Dict -> [(Word,Def)]
dict_toDefs (Dict _ la) = map keyDataToWordDef (la_toListPair la)
	where
		keyDataToWordDef (key, dat) = (key_toString key, data_toString dat)

--
--------------------------------------------------------------------------------
