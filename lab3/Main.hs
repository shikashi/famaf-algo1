module Main ( 
             main 
            )

where

import Dict

import IO

data Action = CreateEmpty | Add Word Def | Delete Word | Search Word | Size
            | Show | Save FilePath | Load FilePath | Quit


clearScreen :: IO [()]
clearScreen = sequence (replicate 80 (putChar '\n'))

printMenu :: IO ()
printMenu = 
    let menu = ["***************************************",
                "* v: Vaciar diccionario               *",
                "* a: Agregar definicion               *",
                "* d: Borrar definicion                *",
                "* b: Buscar definicion                *",
                "* t: Ver el taman~o del diccionario   *",
                "* s: Mostrar diccionario              *",
                "* c: Cargar diccionario desde archivo *",
                "* g: Grabar diccionario a un archivo  *",
                "* q: Salir                            *",
                "***************************************"
               ]
    in do
        sequence (map putStrLn menu)
        return ()

getActionMenu :: IO Action
getActionMenu = 
    let

        getWord :: IO Word
        getWord = do
          putStr "Ingrese la palabra: "
          hFlush stdout
          getLine

        getDef :: IO Def
        getDef = do
          putStr "Ingrese la definicion: "
          hFlush stdout
          getLine

        getNameFile :: IO String
        getNameFile = do
          putStr "Ingrese el nombre de archivo: "
          hFlush stdout
          str <- getLine
          putStrLn ""
          return (str++".dic")

        getAdd, getDelete, getLoad, getSave :: IO Action

        getAdd = do
          w <- getWord
          d <- getDef
          return (Add w d)

        getDelete = do
          w <- getWord
          return (Delete w)

        getSearch = do
          w <- getWord
          return (Search w)

        getLoad = do
          f <- getNameFile
          return (Load f)

        getSave = do
          f <- getNameFile
          return (Save f)
    in do
        printMenu
        putStr "Ingrese una opcion y despues [Intro] "
        c <- getChar
        getChar
        clearScreen
        case c of
          'v' -> return CreateEmpty
          'a' -> getAdd
          'd' -> getDelete
          'b' -> getSearch
          't' -> return Size
          's' -> return Show
          'c' -> getLoad
          'g' -> getSave
          'q' -> return Quit
          _   -> getActionMenu

stepMain :: Dict -> IO (Action,Dict)
stepMain d = 
    let
        showSize :: Dict -> IO()
        showSize d = 
            do 
              putStr "Hay "
              putStr (show (dict_length d))
              putStrLn " palabras en el diccionario\n"

        showSearch :: Word -> Maybe Def -> IO()
        showSearch w Nothing = putStrLn "La palabra no existe\n"
        showSearch w (Just d) = 
            do 
              putStr w
              putStr " : "
              putStrLn d
              putChar '\n'

        showDefs :: [(Word,Def)] -> IO()
        showDefs [] = putChar '\n'
        showDefs ((w,d):ds) = 
            do
              putStr w
              putStr " : "
              putStrLn d
              showDefs ds
    in
      do a <- getActionMenu
         case a of
           CreateEmpty -> return (a, dict_empty)
           Add w df -> return (a, dict_add d w df)
           Delete w -> return (a, dict_del d w)
           Search w -> do showSearch w (dict_search d w) 
                          return (a, d)
           Size ->  do showSize d
                       return (a,d)
           Show ->  do showDefs (dict_toDefs d)
                       return (a,d)
           Load f -> do d' <- dictLoad f
                        return (a,d')
           Save f -> do dictSave d f
                        return (a,d)
           Quit -> return (a,d)


dictLoad :: FilePath -> IO Dict
dictLoad f =
    let

        file2String :: FilePath -> IO String
        file2String f =
            do handle <- openFile f ReadMode
               hGetContents handle -- no carga todo en memoria,
                                   -- es lazy,
                                   -- el archivo se cierra solo,
                                   -- Haskell es muy maestro

        subsecs :: (a -> Bool) -> [a] -> [[a]]
        subsecs p = foldr f [[]]
            where
              f x (ps:pss) | p x       = (x:ps):pss
                           | not (p x) = []:ps:pss
        lineas :: String -> [String]
        lineas str = filter (/=[]) (subsecs (/='\n') str)

        lineToPair :: String -> (Word,Def)
        lineToPair str = case span (/=':') str of
                           ("",_) -> error "Palabra vacia"
                           (w,":") -> error ("Definicion vacia en palabra "
                                             ++ w)
                           (w,"") -> error ("Definicion vacia en palabra "
                                             ++ w)
                           (w, ':':d) -> (w,d)

        dictFromString :: String -> Dict
        dictFromString str = 
            foldr f dict_empty (map lineToPair (lineas str))
                where
                  f (k,d) di = dict_add di k d 
    in
      do
        str <- file2String f
        return (dictFromString str)


dictSave :: Dict -> FilePath -> IO ()
dictSave d f =
    let
        intercal :: a -> [[a]] -> [a]
        intercal x [] = []
        intercal x [ys] = ys
        intercal x (ys:yss) = ys ++ (x: intercal x yss)
  
        defsToString :: [(Word,Def)] -> String
        defsToString wfs = intercal '\n' (map f wfs)
            where
              f (w,d) = w ++ (':':d)
    in
      do
        handle <- openFile f WriteMode
        hPutStr handle (defsToString (dict_toDefs d))
        hClose handle

cycleMain :: Dict -> IO ()
cycleMain d = do
  (a,d') <- stepMain d
  case a of
    Quit -> return ()
    _    -> cycleMain d'

main :: IO ()
main = 
    do 
      clearScreen
      cycleMain dict_empty
      return ()