
--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Abb (
	Abb,
	abb_empty,
	abb_add,
	abb_search,
	abb_del,
	abb_toListPair
) where

--
--------------------------------------------------------------------------------
-- Types

data Abb a b = Hoja | Rama (Abb a b) a b (Abb a b)
	deriving (Show, Eq) -- needed by the testing functions

--
--------------------------------------------------------------------------------
-- Functions

-- abb_empty
-- Arbol vacio
abb_empty :: Abb a b
abb_empty = Hoja

-- abb_add
-- Agrega un elemento al abb.
-- Si ya está el elemento no lo agrega
abb_add :: Ord a => Abb a b -> a -> b -> Abb a b
abb_add Hoja k d = Rama Hoja k d Hoja
abb_add (Rama aiz nk nd ade) k d
	| k == nk = Rama aiz nk nd ade
	| k < nk = Rama (abb_add aiz k d) nk nd ade
	| k > nk = Rama aiz nk nd (abb_add ade k d)

-- abb_search
-- Devuelve el elemento asociado
-- Si no esta devuelve Nothing
abb_search :: Ord a => Abb a b -> a -> Maybe b
abb_search Hoja e = Nothing
abb_search (Rama aiz nk nd ade) e
	| e == nk = Just nd
	| e < nk = abb_search aiz e
	| e > nk = abb_search ade e

-- abb_del
-- Borra un elemento del arbol
-- Si no esta no hace nada
abb_del :: Ord a => Abb a b -> a -> Abb a b
abb_del Hoja _ = Hoja
abb_del (Rama aiz nk nd ade) k
	| k < nk = Rama (abb_del aiz k) nk nd ade
	| k > nk = Rama aiz nk nd (abb_del ade k)
	| k == nk = case (aiz, ade) of
		(Hoja, ade) -> ade
		(aiz, Hoja) -> aiz
		(aiz, ade)  -> Rama subTree subMaxKey subMaxDef ade
	where
		subTern = subMaxAndTreeButMax aiz
		subTree = fst subTern
		subMaxKey = fst (snd subTern)
		subMaxDef = snd (snd subTern)

-- subMaxAndTreeButMax
-- Nunca recibimos una Hoja como argumento, de ese caso nos ocupamos antes.
-- La función se complica un poco si recibimos una Hoja pues tendríamos que
-- decidir qué elemento (a y b) devolver como máximos (nótese: no hay elementos
-- si el subárbol es nada más que una Hoja).
subMaxAndTreeButMax :: Abb a b -> (Abb a b, (a, b))
subMaxAndTreeButMax (Rama aiz nk nd Hoja) = (aiz, (nk, nd))
subMaxAndTreeButMax (Rama aiz nk nd ade) = (Rama aiz nk nd subTree, (subMaxKey, subMaxDef))
	where
		subTern = subMaxAndTreeButMax ade
		subTree = fst subTern
		subMaxKey = fst (snd subTern)
		subMaxDef = snd (snd subTern)

-- test cases
-- (Rama (Rama (Rama Hoja 3 "3" Hoja) 4 "4" (Rama Hoja 6 "6" Hoja)) 7 "7" (Rama Hoja 9 "9" Hoja))
-- (Rama (Rama (Rama Hoja 3 "3" Hoja) 4 "4" (Rama Hoja 16 "16" Hoja)) 27 "27" (Rama Hoja 29 "29" Hoja))

-- abb_toListPair
-- Devuelve la lista de pares correspondiente de los elementos
-- almacenados en el arbol
-- La lista se construye en pre-orden para mantener la ordenación al pasar de la lista
-- de elementos en el fichero a la representación en el árbol y viceversa.
abb_toListPair :: Abb a b -> [(a,b)]
abb_toListPair Hoja = []
abb_toListPair (Rama aiz nk nd ade) = [(nk,nd)] ++ (abb_toListPair aiz) ++ (abb_toListPair ade)

--
--------------------------------------------------------------------------------

