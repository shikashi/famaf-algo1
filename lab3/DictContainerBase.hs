--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module DictContainerBase (
	DictBase,
	Word,
	Def,
	dict_empty,
	dict_add,
	dict_exist,
	dict_search,
	dict_del,
	dict_length,
	dict_toDefs
) where

--
--------------------------------------------------------------------------------
-- Imports

import Data

import Key

import Container

--
--------------------------------------------------------------------------------
-- Types

-- se almacena el tamaño
data DictBase c = DictBase Int (c Key Data)

type Word = String

type Def = String

--
--------------------------------------------------------------------------------
-- Functions

-- dict_empty
-- Crea un diccionario vacío
dict_empty :: Container c => DictBase c
dict_empty = DictBase 0 empty

-- dict_add
-- Agrega un elemento al diccionario
-- Si la clave ya existe, devuelve error
dict_add :: Container c => DictBase c -> Word -> Def -> DictBase c
dict_add (DictBase n co) w d = case search co (key_fromString w) of
	Nothing -> DictBase (n + 1) (add co (key_fromString w) (data_fromString d))
	Just _ -> error "Key already in dictionary."

-- dict_exist
-- Pregunta si la clave existe en el diccionario
dict_exist :: Container c => DictBase c -> Word -> Bool
dict_exist (DictBase 0 _) _ = False
dict_exist (DictBase _ co) w = case search co (key_fromString w) of
	Nothing -> False
	otherwise -> True

-- dict_search
-- Devuelve el dato asociado a una clave
-- Si la clave no existe, devuelve Nothing
dict_search :: Container c => DictBase c -> Word -> Maybe Def
dict_search (DictBase _ co) w = case search co (key_fromString w) of
	Nothing -> Nothing
	Just d -> Just (data_toString d)

-- dict_del
-- Borra un elemento del diccionario a partir de una clave
-- Si la clave no existe, devuelve error
dict_del :: Container c => DictBase c -> Word -> DictBase c
dict_del (DictBase n co) w = case search co (key_fromString w) of
	Nothing -> error "Key does not exist."
	Just _ -> DictBase (n - 1) (del co (key_fromString w))

-- dict_length
-- Devuelve la cantidad de elementos en el diccionario
dict_length :: Container c => DictBase c -> Int
dict_length (DictBase n _) = n

-- dict_toDefs
-- Devuelve las definiciones del diccionario
dict_toDefs :: Container c => DictBase c -> [(Word,Def)]
dict_toDefs (DictBase _ co) = map keyDataToWordDef (toListPair co)
	where
		keyDataToWordDef (key, dat) = (key_toString key, data_toString dat)

--
--------------------------------------------------------------------------------
