--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Utils where

--
--------------------------------------------------------------------------------
-- Imports

import System.CPUTime ( cpuTimePrecision )

--
--------------------------------------------------------------------------------
-- Functions

-- foldr3
-- Useful to fold a list of pairs into a ListAssoc or Abb.
foldr3 :: (a -> b -> c -> c) -> c -> [(a,b)] -> c
foldr3 _ z [] = z
foldr3 f z (x:xs) = f (fst x) (snd x) $ foldr3 f z xs

-- foldl3
-- Useful to fold a list of pairs into a ListAssoc or Abb.
foldl3 :: (c -> a -> b -> c) -> c -> [(a,b)] -> c
foldl3 _ z [] = z
foldl3 f z (x:xs) = foldl3 f (f z (fst x) (snd x)) xs

-- timeThunk
-- Calculates the time offset in the evaluation of the thunk in monad thunkM.
-- Uses monad timeM to get timestamp information. The result of the evaluation
-- of the thunk is discarded and the time offset is returned (in monad m).
--timeThunk :: Num a => IO a -> IO b -> IO a
timeThunk :: (Monad m, Num a) => m a -> m b -> m a
timeThunk timeM thunkM = do
	t1 <- timeM
	x <- thunkM
	return $! x
	t2 <- timeM
	return $ t2 - t1

-- timeAndThunk
-- Like timeThunk but returns a pair containing the time offset and the result
-- of the evaluation of the thunk.
--timeAndThunk :: Num a => IO a -> IO b -> IO (a,b)
timeAndThunk :: (Monad m, Num a) => m a -> m b -> m (a,b)
timeAndThunk timeM thunkM = do
	t1 <- timeM
	x <- thunkM
	return $! x
	t2 <- timeM
	return $ (t2 - t1, x)

-- fromPico
-- Utility to convert, for instance, picoseconds to seconds.
-- 10^12 pico-sth = 1 sth
fromPico :: (Integral a, Fractional b) => a -> b
fromPico pico = fromIntegral pico / fromIntegral 1000000000000

-- cpuTimePrecisionPlacesSecs
-- Compute the number of significative decimal digits of getCPUTime values in
-- seconds and based on the result of cpuTimePrecision.
cpuTimePrecisionPlacesSecs :: Integer
cpuTimePrecisionPlacesSecs =
	ceiling $ logBase 10 $	fromIntegral 10^12 / fromIntegral cpuTimePrecision

--
--------------------------------------------------------------------------------
