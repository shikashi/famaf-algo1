--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module ListAssocOrdContainer (
	ListAssoc,
	Container(..)
) where

--
--------------------------------------------------------------------------------
-- Imports

import ListAssocOrd

import Container

--
--------------------------------------------------------------------------------
-- Class membership

instance Container ListAssoc where
	empty = la_empty
	add = la_add
	search = la_search
	del = la_del
	toListPair = la_toListPair

--
--------------------------------------------------------------------------------
