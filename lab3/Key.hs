--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module Key (
	Key,
	key_MaxLen,
	key_fromString,
	key_toString,
	key_length
) where

--
--------------------------------------------------------------------------------
-- Types

data Key = KeyVal String Int
	deriving Show  -- needed by the testing functions

--
--------------------------------------------------------------------------------
-- Functions

-- key_MaxLen
-- Máximo tamaño de la clave
key_MaxLen :: Int
key_MaxLen = 50

-- key_fromString
-- De un String construye una clave
-- El tamaño del String debe ser menor o igual a key_MaxLen
key_fromString :: String -> Key
key_fromString str
	| strlen > key_MaxLen = error "Excessive string length"
	| otherwise = KeyVal str strlen
	where
		strlen = length str

-- key_toString
-- Convierte una clave a String
key_toString :: Key -> String
key_toString (KeyVal str _) = str

-- key_length
-- Devuelve el tamaño de la clave
key_length :: Key -> Int
key_length (KeyVal _ len) = len

--
--------------------------------------------------------------------------------
-- Class membership

instance Eq Key where
	(KeyVal str1 len1) == (KeyVal str2 len2) = len1 == len2 && str1 == str2

-- Por ahora la comparación es estrictamente lexicográfica sobre la cadena.
instance Ord Key where
	(KeyVal str1 len1) <= (KeyVal str2 len2) = str1 < str2 || (KeyVal str1 len1) == (KeyVal str2 len2)
{-
	compare k1@(KeyVal str1 _) k2@(KeyVal str2 _)
		| str1 < str2 = LT
		| k1 == k2 = EQ
		| otherwise = GT
-}

--
--------------------------------------------------------------------------------
