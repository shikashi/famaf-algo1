--------------------------------------------------------------------------------
-- Proyecto 3
-- Grupo 12
--------------------------------------------------------------------------------

module ListAssoc (
	ListAssoc,
	la_empty,
	la_add,
	la_search,
	la_del,
	la_toListPair
) where

--
--------------------------------------------------------------------------------
-- Imports

-- Declaración del tipo ListAssoc.
import ListAssocType

--
--------------------------------------------------------------------------------
-- Functions

-- la_empty
-- Lista de asociaciones vacía
la_empty :: ListAssoc a b
la_empty = Empty

-- la_add
-- Agrega un elemento a una lista de asociaciones
-- Si el elemento ya existe, devuelve la misma lista
-- Nota: dos elementos son iguales cuando la primera componente de cada elemento
-- coincide.
la_add :: Eq a => ListAssoc a b -> a -> b -> ListAssoc a b
la_add Empty x y = Node x y Empty
la_add (Node u v next) x y
	| x == u = Node u v next
	| otherwise = Node u v (la_add next x y)

-- la_search
-- Devuelve el elemento asociado
-- Si el elemento no existe, devuelve Nothing
la_search :: Eq a => ListAssoc a b -> a -> Maybe b
la_search Empty _ = Nothing
la_search (Node u v next) x
	| x == u = Just v
	| otherwise = la_search next x

-- la_del
-- Borra un elemento de la lista de asociaciones
-- Si el elemento no existe, hace nada
la_del :: Eq a => ListAssoc a b -> a -> ListAssoc a b
la_del Empty _ = Empty
la_del (Node u v next) x
	| x == u = next
	| otherwise = Node u v (la_del next x)

-- la_toListPair
-- Devuelve la lista de pares correspondiente a la lista de asociaciones
la_toListPair :: ListAssoc a b -> [(a,b)]
la_toListPair Empty = []
la_toListPair (Node u v next) = (u,v) : la_toListPair next

--
--------------------------------------------------------------------------------
