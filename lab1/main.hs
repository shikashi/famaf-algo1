module Main where

----------------------------------
-- Proyecto 1
----------------------------------
-- Grupo nº 12


----------------------------------
-- 1)

-- 1) a)
-- paratodo
paratodo :: (a -> Bool) -> [a] -> Bool
paratodo p [] = True
paratodo p (x:xs) = p x && paratodo p xs

-- 1) b)
-- existe
existe :: (a -> Bool) -> [a] -> Bool
existe p [] = False
existe p (x:xs) = p x || existe p xs

-- 1) c)
-- sumatoria
sumatoria :: (a -> Integer) -> [a] -> Integer
sumatoria f [] = 0
sumatoria f (x:xs) = f x + sumatoria f xs

-- 1) d)
-- productoria
productoria :: (a -> Integer) -> [a] -> Integer
productoria f [] = 1
productoria f (x:xs) = f x * productoria f xs

-- 1) e)
-- cantidad
cantidad :: (a -> Bool) -> [a] -> Integer
cantidad p [] = 0
cantidad p (x:xs) | p x = 1 + cantidad p xs
                  | otherwise = cantidad p xs

cantidad' :: (a -> Bool) -> [a] -> Integer
cantidad' p xs = sumatoria vale xs
    where
          vale x | p x = 1
                 | otherwise = 0

-- Punto * 1
-- cantidad''
cantidad'' :: (a -> Bool) -> [a] -> Integer
cantidad'' p = sumatoria vale
	where
		vale x | p x = 1
		       | otherwise = 0

-- Punto * 2
-- sumatoria'
sumatoria' :: Num b => (a -> b) -> [a] -> b
sumatoria' f [] = 0
sumatoria' f (x:xs) = f x + sumatoria' f xs

-- productoria'
productoria' :: Num b => (a -> b) -> [a] -> b
productoria' f [] = 1
productoria' f (x:xs) = f x * productoria' f xs

-- cantidad'''
cantidad''' :: Num b => (a -> Bool) -> [a] -> b
cantidad''' p [] = 0
cantidad''' p (x:xs) | p x = 1 + cantidad''' p xs
		      | otherwise = cantidad''' p xs


----------------------------------


----------------------------------
-- 2)

-- 2) i)
-- todosPares
todosPares :: [Integer] -> Bool
todosPares xs = paratodo esPar xs
    where esPar k = mod k 2 == 0

-- 2) ii)
-- hayMultiplo
hayMultiplo :: Integer -> [Integer] -> Bool
hayMultiplo d xs = existe esMultiplo xs
    where esMultiplo k = mod k d == 0

-- 2) iii)
-- sumaCuadrados
sumaCuadrados :: Integer -> Integer
sumaCuadrados n = sumatoria (^2) [0..n-1]

-- 2) iv)
-- cantImpar
cantImpar :: [Integer] -> Integer
cantImpar xs = cantidad esImpar xs
    where esImpar k = mod k 2 == 1

--
----------------------------------


----------------------------------
-- 3)

-- 3) a)
-- cuantGen
cuantGen :: (a -> b -> b) -> b -> (c -> a) -> [c] -> b
cuantGen op z f [] = z
cuantGen op z f (x:xs) = op (f x) (cuantGen op z f xs)

-- 3) b)
-- genTodosPares
genTodosPares :: [Integer] -> Bool
genTodosPares xs = cuantGen (&&) True esPar xs
	where esPar k = mod k 2 == 0

-- 3) b)
-- genHayMultiplo
genHayMultiplo :: Integer -> [Integer] -> Bool
genHayMultiplo d xs = cuantGen (||) False esMultiplo xs
	where esMultiplo k = mod k d == 0

-- 3) b)
-- genSumaCuadrados
genSumaCuadrados :: Integer -> Integer
genSumaCuadrados n = cuantGen (+) 0 (^2) [0..n-1]

-- 3) b)
-- genCantImpar
genCantImpar :: [Integer] -> Integer
genCantImpar xs = cuantGen (+) 0 vale xs
	where
		vale c | mod c 2 == 1 = 1
		       | otherwise = 0

-- Punto * 3

-- 3*) b) i)
-- genTodosPares'
genTodosPares' :: [Integer] -> Bool
genTodosPares' = cuantGen (&&) True esPar 
	where esPar k = mod k 2 == 0

-- 3*) b) ii)
-- genHayMultiplo'
genHayMultiplo' :: Integer -> [Integer] -> Bool
genHayMultiplo' d = cuantGen (||) False esMultiplo 
	where esMultiplo k = mod k d == 0

-- 3*) b) iii)
-- genSumaCuadrados'
genSumaCuadrados' :: Integer -> Integer
genSumaCuadrados' n = cuantGen (+) 0 (^2) [0..n-1]

-- 3*) b) iv)
-- genCantImpar'
genCantImpar' :: [Integer] -> Integer
genCantImpar' = cuantGen (+) 0 vale 
	where
		vale c | mod c 2 == 1 = 1
		       | otherwise = 0

-- Punto * 4
-- cuantGen'
cuantGen' :: (a -> b -> b) -> b -> (c -> a) -> [c] -> b
cuantGen' op z f xs = foldr op z (map f xs)

--
----------------------------------


----------------------------------
-- 4)

-- 4) a) i)
-- sumarALista
sumarALista :: Num a => a -> [a] -> [a]
sumarALista s [] = []
sumarALista s (x:xs) = (s+x) : sumarALista s xs

-- 4) a) ii)
-- encabezar
encabezar :: a -> [[a]] -> [[a]]
encabezar e [] = []
encabezar e (xs:xss) = (e:xs) : encabezar e xss

-- 4) b) i)
-- sumarAListaMap
sumarAListaMap :: Num a => a -> [a] -> [a]
sumarAListaMap s xs = map (s+) xs

-- 4) b) ii)
-- encabezarMap
encabezarMap :: a -> [[a]] -> [[a]]
encabezarMap s xss = map (s:) xss

--
----------------------------------


----------------------------------
-- 5)

-- 5) a)
-- devuelvePares
devuelvePares :: [Integer] -> [Integer]
devuelvePares [] = []
devuelvePares (x:xs) | mod x 2 == 0 = x : devuelvePares xs
                     | otherwise = devuelvePares xs

-- 5) b)
-- devuelvePares'
devuelvePares' :: [Integer] -> [Integer]
devuelvePares' xs = filter esPar xs
    where esPar c = mod c 2 == 0

--
----------------------------------


----------------------------------
--

-- 6) a)
-- encuentra
encuentra :: Integer -> [(Integer, String)] -> String
encuentra n [] = ""
encuentra n ((a,b):xs) | n==a = b
                       | otherwise = encuentra n xs

-- 6) b)
-- encuentraFilter
encuentraFilter :: Integer -> [(Integer, String)] -> String
encuentraFilter n xs = case filter compara xs of
                             [] -> ""
                             ((_, b):xs) -> b
    where
        compara (a,_) = n == a

encuentraFilter' :: Integer -> [(Integer, String)] -> String
encuentraFilter' n xs = wrapper (filter compara xs)
    where
          wrapper [] = ""
          wrapper ((_,b):xs) = b
          compara (a,_) = n == a

{-
-- Extra
encuentraFilter'' :: Integer -> [(Integer, String)] -> String
encuentraFilter'' n xs = snd ((filter compara (xs ++ [(n, "")])) !! 0)
    where
        compara (a,_) = n == a
-}

--
----------------------------------


----------------------------------
--

-- 7) a)
-- primIgualesA
primIgualesA :: Eq a => a -> [a] -> [a] 
primIgualesA c [] = []
primIgualesA c (x:xs) | x == c = x : primIgualesA c xs
		      |	otherwise = []

-- 7) b)
-- primIgualesAtW
primIgualesAtW :: Eq a => a -> [a] -> [a]
primIgualesAtW p xs = takeWhile (p==) xs

--
----------------------------------


----------------------------------
--

-- 8) a)
-- contarLa
contarLa :: String -> Integer
contarLa [] = 0
contarLa [e] = 0
contarLa (x:y:xs) | (x == 'l' && y == 'a') = 1 + contarLa xs 
		  | otherwise = contarLa (y:xs)
		

-- 8) b)
-- contarLas
contarLas :: String -> Integer        
contarLas [] = 0
contarLas (x:[]) = 0
contarLas (x:y:[]) = 0
contarLas (x:y:z:xs) | (x == 'l' && y == 'a' && z == 's') = 1 + contarLas xs 
                     | otherwise = contarLas (y:z:xs)

--
----------------------------------


----------------------------------
--

-- 9) a)
-- minimo
minimo :: Ord a => [a] -> a
minimo [x] = x
minimo (x:y:xs) | x <= y = minimo (x:xs)
		| otherwise = minimo (y:xs)

-- 9) b)
-- minimo'
minimo' :: (Ord a, Bounded a) => [a] -> a
minimo' [] = maxBound
minimo' (x:xs) | x <= mini = x
	       | otherwise = mini
	where
		mini = minimo' xs

--
----------------------------------

